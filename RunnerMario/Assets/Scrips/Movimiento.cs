using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public int velocidad = 5;
    public bool enSuelo = true; //Si mario esta colisionando con un suelo para poder saltar
    public delegate void dead(); // Delegado y evento que se llaman cuando mario muere
    public static event dead muerte;
    public bool estaMuerto = false;
    
    void Start()
    {
        estaMuerto = false;
    }

    void Update()
    {
        //Movimiento con axis 
        this.GetComponent<Rigidbody2D>().velocity = new Vector3(Input.GetAxis("Horizontal") * velocidad, this.GetComponent<Rigidbody2D>().velocity.y, 0);
        //Salto
        if (Input.GetKeyDown("space") && enSuelo)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0,500));
            enSuelo = false;
        }
        //Si sale de estos limites acaba el juego
        if ((this.transform.position.y <= -10 || this.transform.position.x <= -11) && !estaMuerto)
        {
            estaMuerto = true;
            if (muerte != null)
            {
                muerte();
            }
            Invoke("gameOver", 4f);   
        }
    }

    //Se comprueba que este en el suelo y cuando colisiona con el y se transforma en hijo del suelo para moverse con la plataforma
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Suelo" && !enSuelo)
        {
            enSuelo = true;
            this.transform.parent = collision.transform;
        }
    }
    //Se comprueba si entra
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Suelo")
        {
            enSuelo = true;
            this.transform.parent = collision.transform;
        }
    }
    //Se comprueba si sale
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Suelo")
        {
            this.transform.parent = null;
        }
    }
    //Recarga la escena
    public void gameOver()
    {
        SceneManager.LoadScene("MarianoAlone");
    }
}
