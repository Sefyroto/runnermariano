using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public float frequencia = 3f;

    //Invocar los spawners y cada x tiempo baja la frecuencia
    void Start()
    {
        InvokeRepeating("spawnLadrillo", 0f, frequencia);
        InvokeRepeating("incrementoVelocidad", 5f, 10f);
    }

    // Genera el ladrillo
    public void spawnLadrillo()
    {
        float r = Random.Range(-4.5f, 3f);
        Vector3 posicion = new Vector3(this.transform.position.x, r, 0);
        GameObject ladrillo = PoolLadrillos.Instance.sacar();
        ladrillo.transform.position = posicion;
    }

    // Sube la velocidad de spawn
    public void incrementoVelocidad()
    {
        CancelInvoke("spawnLadrillo");
        frequencia -= 0.1f;
        InvokeRepeating("spawnLadrillo", 0f, frequencia);
    }
}
