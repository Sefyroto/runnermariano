using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoIzquierda : MonoBehaviour
{
    public float velocidad;
    
    //Componente de movimiento lateral
    void Update()
    {
        transform.position = transform.position + new Vector3(-velocidad * Time.deltaTime, 0f);
    }
}
