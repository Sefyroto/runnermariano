using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReSpawnArboles : MonoBehaviour
{
    public float posInicial; //Posicion inicial para spawnear los arboles
    public float posFinal; // Posicion final donde recojere los arboles

    //Cuando los arboles llegan al final spawnean al principio
    void Update()
    {
        if (this.transform.position.x <= posFinal)
        {
            float y = this.transform.position.y;
            this.transform.position = new Vector3(posInicial,y,0f);
        }
    }
}
