using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UiPuntuacion : MonoBehaviour
{
    Text puntos;

    //Se suscribe
    void Start()
    {
        puntos = GetComponent<Text>();
        GameManager.puntos += actualizarPuntos;
    }

    //Actualiza los puntos de la ui
    public void actualizarPuntos(int puntuacion)
    {
        puntos.text = "Puntos: " + puntuacion;
    }

    //Se desuscribe para que no pete unity
    private void OnDestroy()
    {
        GameManager.puntos -= actualizarPuntos;
    }

}
