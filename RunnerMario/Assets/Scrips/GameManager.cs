using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    int segundos = 0;
    bool jugando = true; // variable para mirar si el juego esta en marcha
    public AudioClip sonidoMuerte;
    public delegate void tiempo(int segundos); //Delegado y evento de tiempo para contar segundo a segundo
    public static event tiempo puntos; 

    void Start()
    {
        StartCoroutine(timer()); 
        Movimiento.muerte += cambioAudio;
    }
    
    IEnumerator timer() // Cada segundo aumenta en 1 los puntos
    {
        while (jugando)
        {
            yield return new WaitForSeconds(1f);
            segundos++;
            if (puntos != null)
            {
                puntos(segundos);
            }
        }
    }

    public void cambioAudio() // Reproduce el sonido de Game Over y cambia el jugar a falso para terminar el juego
    {
        jugando = false;
        GetComponent<AudioSource>().clip = sonidoMuerte;
        GetComponent<AudioSource>().Play(); 
    }

    private void OnDestroy() // Se desuscribe para que no pete
    {
        Movimiento.muerte -= cambioAudio;
    }
}
