using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    GameObject fondoGameOver; // Pantalla Game Over
    
    void Start()
    {
        Movimiento.muerte += activarGameOver;
        fondoGameOver = transform.Find("GameOver").gameObject;
    }

    public void activarGameOver() //Activa la pantalla de GameOver
    {
        fondoGameOver.SetActive(true);
    }

    private void OnDestroy()
    {
        Movimiento.muerte -= activarGameOver;
    }
}
