using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolLadrillos : MonoBehaviour
{
    public List<GameObject> prefab = new List<GameObject>(); //Lista de prefabs
    private static PoolLadrillos instance; //Singelton
    private List<GameObject> listaPool; //Lista pool

    public static PoolLadrillos Instance { get => instance; private set => instance = value; } //Geter Seter del singelton

    private void Awake() // Comprueba si tiene la instancia y si no la crea
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start()
    {
        listaPool = new List<GameObject>();
        crear(10);
    }

    public void meter(GameObject obj) //Desactiva los objetos " los devuelve al pool "
    {

        if (listaPool.Contains(obj))
        {
            obj.SetActive(false);
        }
    }

    public GameObject sacar() //Activa los objetos " los saca del pool "
    {
        foreach (GameObject obj in listaPool)
        {
            if (!obj.activeInHierarchy)
            {
                obj.SetActive(true);
                return obj;
            }
        }
        return null;
    }

    public void agregar() //Crea objetos y los mete dentro del pool
    {
        int indice = Random.Range(0, prefab.Count);
        GameObject obj = Instantiate(prefab[indice]);
        obj.SetActive(false);
        listaPool.Add(obj);
    }

    public void crear(int num) // Crea la cantidad de objetos en el pool
    {
        for (int i = 0; i < num; i++)
        {
            agregar();
        }
    }
}
