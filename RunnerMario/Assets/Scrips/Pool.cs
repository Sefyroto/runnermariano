using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    public GameObject prefab; //Prefab del pool
    private List<GameObject> listaPool = new List<GameObject>(); //Lista de objetos

    public void meter(GameObject obj) //Desactiva los objetos " los devuelve al pool "
    {

        if (listaPool.Contains(obj))
        {
            obj.SetActive(false);
        }
    }

    public GameObject sacar() //Activa los objetos " los saca del pool "
    {
        foreach (GameObject obj in listaPool) 
        {
            if (!obj.activeInHierarchy)
            {
                obj.SetActive(true);
                return obj;
            }
        }
        return null;
    }

    public void agregar() //Crea objetos y los mete dentro del pool
    {
        GameObject obj = Instantiate(prefab);
        obj.SetActive(false);
        listaPool.Add(obj);
    }

    public void crear(int num) // Crea la cantidad de objetos en el pool
    {
        for (int i = 0; i < num; i++)
        {
            agregar();
        }
    }
}
